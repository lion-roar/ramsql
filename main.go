package main

import (
	"database/sql"
	"fmt"

	"gitee.com/lion-roar/ramsql/cli"
	_ "gitee.com/lion-roar/ramsql/driver"
)

func main() {
	db, err := sql.Open("ramsql", "")
	if err != nil {
		fmt.Printf("Error : cannot open connection : %s\n", err)
		return
	}
	cli.Run(db)
}
